from tkinter import *
from tkinter import ttk

import pendulum


COMMON_FORMATS_MAP = {
    "ATOM": "YYYY-MM-DD[T]HH:mm:ssZ",
    "COOKIE": "dddd, DD-MMM-YYYY HH:mm:ss zz",
    "ISO8601": "YYYY-MM-DD[T]HH:mm:ssZZ",
    "RFC822": "ddd, D MMM YY HH:mm:ss ZZ",
    "RFC850": "dddd, DD-MMM-YY HH:mm:ss zz",
    "RFC1036": "ddd, DD MMM YY HH:mm:ss ZZ",
    "RFC1123": "ddd, D MMM YYYY HH:mm:ss ZZ",
    "RFC2822": "ddd, DD MMM YYYY HH:mm:ss ZZ",
    "RFC3339": "YYYY-MM-DD[T]HH:mm:ssZZ",
    "RSS": "ddd, DD MMM YYYY HH:mm:ss zz",
    "W3C": "YYYY-MM-DD[T]HH:mm:ssZ",
}
COMMON_FORMATS = sorted(list(COMMON_FORMATS_MAP.keys()) + ["MILLISECONDS", "SECONDS"])

EPOCH = pendulum.from_timestamp(0)


def epoch_seconds(dt):
    return (dt - EPOCH).total_seconds()


def perform_conversion(*_args):
    in_fmt_key = in_fmt_var.get()
    out_fmt_key = out_fmt_var.get()
    input_val = input_var.get()
    tz = timezone_var.get()

    if in_fmt_key in COMMON_FORMATS_MAP:
        in_fmt = COMMON_FORMATS_MAP[in_fmt_key]
        dt = pendulum.from_format(input_val, in_fmt)
    elif in_fmt_key == "MILLISECONDS":
        dt = pendulum.from_timestamp(int(input_val) / 1000, tz=tz)
    elif in_fmt_key == "SECONDS":
        dt = pendulum.from_timestamp(int(input_val), tz=tz)
    else:
        raise

    if out_fmt_key in COMMON_FORMATS_MAP:
        out_fmt = COMMON_FORMATS_MAP[out_fmt_key]
        out_str = dt.format(out_fmt)
    elif out_fmt_key == "MILLISECONDS":
        out_str = str(int(epoch_seconds(dt)) * 1000)
    elif out_fmt_key == "SECONDS":
        out_str = str(int(epoch_seconds(dt)))
    else:
        raise

    output_var.set(out_str)


root = Tk()
root.title("Pendulum Tk")
mainframe = ttk.Frame(root, padding="20")

input_var = StringVar()
output_var = StringVar()
in_fmt_var = StringVar()
out_fmt_var = StringVar()
timezone_var = StringVar()

input_label = ttk.Label(mainframe, text="Timestamp:")
input_entry = ttk.Entry(mainframe, textvariable=input_var)

in_fmt_label = ttk.Label(mainframe, text="In:")
in_fmt_select = ttk.Combobox(mainframe, textvariable=in_fmt_var)
in_fmt_select["values"] = COMMON_FORMATS

out_fmt_label = ttk.Label(mainframe, text="Out:")
out_fmt_select = ttk.Combobox(mainframe, textvariable=out_fmt_var)
out_fmt_select["values"] = COMMON_FORMATS

timezone_label = ttk.Label(mainframe, text="Timezone:")
timezone_select = ttk.Combobox(mainframe, textvariable=timezone_var)
timezone_select["values"] = pendulum.timezones
timezone_select.state(["readonly"])

style = ttk.Style()
style.configure("BG.TLabel", foreground="black", background="grey")
output_entry = ttk.Entry(
    mainframe, textvariable=output_var, state="readonly", style="BG.TLabel", width=30
)

mainframe.grid(column=0, row=0, columnspan=2, sticky="nwes")
input_label.grid(column=0, row=0, sticky="e")
input_entry.grid(column=1, row=0)

in_fmt_label.grid(column=0, row=1, sticky="e")
in_fmt_select.grid(column=1, row=1)

out_fmt_label.grid(column=0, row=2, sticky="e")
out_fmt_select.grid(column=1, row=2)

timezone_label.grid(column=0, row=3, sticky="e")
timezone_select.grid(column=1, row=3)

output_entry.grid(column=0, row=4, columnspan=2, pady="20")

input_entry.focus()
root.bind("<Return>", perform_conversion)

root.mainloop()
